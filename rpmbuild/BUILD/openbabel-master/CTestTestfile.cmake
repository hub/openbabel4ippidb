# CMake generated Testfile for 
# Source directory: /home/tru/rpmbuild/BUILD/openbabel-master
# Build directory: /home/tru/rpmbuild/BUILD/openbabel-master
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
SUBDIRS(include)
SUBDIRS(data)
SUBDIRS(doc)
SUBDIRS(src)
SUBDIRS(tools)
SUBDIRS(src/GUI)
SUBDIRS(test)
SUBDIRS(scripts)
