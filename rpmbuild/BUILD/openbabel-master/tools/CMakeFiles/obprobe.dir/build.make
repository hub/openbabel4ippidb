# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Produce verbose output by default.
VERBOSE = 1

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/tru/rpmbuild/BUILD/openbabel-master

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/tru/rpmbuild/BUILD/openbabel-master

# Include any dependencies generated for this target.
include tools/CMakeFiles/obprobe.dir/depend.make

# Include the progress variables for this target.
include tools/CMakeFiles/obprobe.dir/progress.make

# Include the compile flags for this target's objects.
include tools/CMakeFiles/obprobe.dir/flags.make

tools/CMakeFiles/obprobe.dir/obprobe.cpp.o: tools/CMakeFiles/obprobe.dir/flags.make
tools/CMakeFiles/obprobe.dir/obprobe.cpp.o: tools/obprobe.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/tru/rpmbuild/BUILD/openbabel-master/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object tools/CMakeFiles/obprobe.dir/obprobe.cpp.o"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/tools && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/obprobe.dir/obprobe.cpp.o -c /home/tru/rpmbuild/BUILD/openbabel-master/tools/obprobe.cpp

tools/CMakeFiles/obprobe.dir/obprobe.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/obprobe.dir/obprobe.cpp.i"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/tools && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/tru/rpmbuild/BUILD/openbabel-master/tools/obprobe.cpp > CMakeFiles/obprobe.dir/obprobe.cpp.i

tools/CMakeFiles/obprobe.dir/obprobe.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/obprobe.dir/obprobe.cpp.s"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/tools && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/tru/rpmbuild/BUILD/openbabel-master/tools/obprobe.cpp -o CMakeFiles/obprobe.dir/obprobe.cpp.s

tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.requires:
.PHONY : tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.requires

tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.provides: tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.requires
	$(MAKE) -f tools/CMakeFiles/obprobe.dir/build.make tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.provides.build
.PHONY : tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.provides

tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.provides.build: tools/CMakeFiles/obprobe.dir/obprobe.cpp.o

# Object files for target obprobe
obprobe_OBJECTS = \
"CMakeFiles/obprobe.dir/obprobe.cpp.o"

# External object files for target obprobe
obprobe_EXTERNAL_OBJECTS =

bin/obprobe: tools/CMakeFiles/obprobe.dir/obprobe.cpp.o
bin/obprobe: tools/CMakeFiles/obprobe.dir/build.make
bin/obprobe: lib64/libopenbabel.so.5.0.0
bin/obprobe: /usr/lib64/libm.so
bin/obprobe: /usr/lib64/libz.so
bin/obprobe: tools/CMakeFiles/obprobe.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX executable ../bin/obprobe"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/tools && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/obprobe.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
tools/CMakeFiles/obprobe.dir/build: bin/obprobe
.PHONY : tools/CMakeFiles/obprobe.dir/build

tools/CMakeFiles/obprobe.dir/requires: tools/CMakeFiles/obprobe.dir/obprobe.cpp.o.requires
.PHONY : tools/CMakeFiles/obprobe.dir/requires

tools/CMakeFiles/obprobe.dir/clean:
	cd /home/tru/rpmbuild/BUILD/openbabel-master/tools && $(CMAKE_COMMAND) -P CMakeFiles/obprobe.dir/cmake_clean.cmake
.PHONY : tools/CMakeFiles/obprobe.dir/clean

tools/CMakeFiles/obprobe.dir/depend:
	cd /home/tru/rpmbuild/BUILD/openbabel-master && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/tru/rpmbuild/BUILD/openbabel-master /home/tru/rpmbuild/BUILD/openbabel-master/tools /home/tru/rpmbuild/BUILD/openbabel-master /home/tru/rpmbuild/BUILD/openbabel-master/tools /home/tru/rpmbuild/BUILD/openbabel-master/tools/CMakeFiles/obprobe.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : tools/CMakeFiles/obprobe.dir/depend

