# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/tru/rpmbuild/BUILD/openbabel-master/tools/obthermo.cpp" "/home/tru/rpmbuild/BUILD/openbabel-master/tools/CMakeFiles/obthermo.dir/obthermo.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_EIGEN"
  "HAVE_EIGEN3"
  "HAVE_LIBZ"
  "HAVE_RADPIJSON"
  "HAVE_SHARED_POINTER"
  "HAVE_WXWIDGETS"
  "USING_DYNAMIC_LIBS"
  "_FILE_OFFSET_BITS=64"
  "_LARGE_FILES"
  "__WXGTK__"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/tru/rpmbuild/BUILD/openbabel-master/src/CMakeFiles/openbabel.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "/usr/lib64/wx/include/gtk2-unicode-release-2.8"
  "/usr/include/wx-2.8"
  "include"
  "data"
  "/usr/include/eigen3"
  "external/rapidjson-1.1.0/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
