# Install script for directory: /home/tru/rpmbuild/BUILD/openbabel-master/include

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel" TYPE FILE FILES "/home/tru/rpmbuild/BUILD/openbabel-master/include/chemdrawcdx.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/alias.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/atom.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/base.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/bitvec.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/bond.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/bondtyper.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/builder.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/canon.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/chains.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/chargemodel.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/chiral.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/conformersearch.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/data.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/data_utilities.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/descriptor.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/distgeom.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/dlhandler.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/elements.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/fingerprint.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/forcefield.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/format.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/generic.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/graphsym.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/grid.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/griddata.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/groupcontrib.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/inchiformat.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/internalcoord.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/isomorphism.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/json.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/kekulize.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/kinetics.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/lineend.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/locale.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/matrix.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/mcdlutil.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/mol.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/molchrg.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/obconversion.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/oberror.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/obfunctions.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/obiter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/obmolecformat.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/obutil.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/op.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/optransform.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/parsmart.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/patty.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/phmodel.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/plugin.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/pointgroup.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/query.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/rand.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/reaction.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/reactionfacade.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/residue.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/ring.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/rotamer.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/rotor.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/shared_ptr.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/spectrophore.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/tautomer.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/text.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/tokenst.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/typer.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/xml.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/babelconfig.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel/math" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/math/align.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/math/erf.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/math/matrix3x3.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/math/spacegroup.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/math/transform3d.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/math/vector3.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel/stereo" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/bindings.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/cistrans.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/squareplanar.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/stereo.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/tetrahedral.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/tetranonplanar.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/stereo/tetraplanar.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel/depict" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/depict/asciipainter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/depict/cairopainter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/depict/commandpainter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/depict/depict.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/depict/painter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/include/openbabel/depict/svgpainter.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

