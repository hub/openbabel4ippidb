# Install script for directory: /home/tru/rpmbuild/BUILD/openbabel-master/doc

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/man/man1/babel.1;/usr/share/man/man1/obabel.1;/usr/share/man/man1/obchiral.1;/usr/share/man/man1/obconformer.1;/usr/share/man/man1/obdistgen.1;/usr/share/man/man1/obenergy.1;/usr/share/man/man1/obfit.1;/usr/share/man/man1/obgen.1;/usr/share/man/man1/obgrep.1;/usr/share/man/man1/obgui.1;/usr/share/man/man1/obminimize.1;/usr/share/man/man1/obprobe.1;/usr/share/man/man1/obprop.1;/usr/share/man/man1/obrms.1;/usr/share/man/man1/obrotamer.1;/usr/share/man/man1/obrotate.1;/usr/share/man/man1/obspectrophore.1;/usr/share/man/man1/obsym.1;/usr/share/man/man1/obtautomer.1;/usr/share/man/man1/obthermo.1;/usr/share/man/man1/roundtrip.1")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/share/man/man1" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/babel.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obabel.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obchiral.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obconformer.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obdistgen.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obenergy.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obfit.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obgen.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obgrep.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obgui.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obminimize.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obprobe.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obprop.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obrms.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obrotamer.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obrotate.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obspectrophore.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obsym.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obtautomer.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/obthermo.1"
    "/home/tru/rpmbuild/BUILD/openbabel-master/doc/roundtrip.1"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/openbabel/2.4.90/splash.png")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/share/openbabel/2.4.90" TYPE FILE FILES "/home/tru/rpmbuild/BUILD/openbabel-master/doc/splash.png")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

