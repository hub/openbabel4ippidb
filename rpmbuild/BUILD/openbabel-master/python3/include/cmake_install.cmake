# Install script for directory: /home/tru/rpmbuild/BUILD/openbabel-master/python3/include

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel" TYPE FILE FILES "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/chemdrawcdx.h")
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/alias.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/atom.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/base.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/bitvec.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/bond.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/bondtyper.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/builder.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/canon.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/chains.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/chargemodel.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/chiral.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/conformersearch.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/data.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/data_utilities.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/descriptor.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/distgeom.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/dlhandler.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/elements.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/fingerprint.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/forcefield.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/format.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/generic.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/graphsym.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/grid.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/griddata.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/groupcontrib.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/inchiformat.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/internalcoord.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/isomorphism.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/json.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/kekulize.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/kinetics.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/lineend.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/locale.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/matrix.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/mcdlutil.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/mol.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/molchrg.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/obconversion.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/oberror.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/obfunctions.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/obiter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/obmolecformat.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/obutil.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/op.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/optransform.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/parsmart.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/patty.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/phmodel.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/plugin.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/pointgroup.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/query.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/rand.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/reaction.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/reactionfacade.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/residue.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/ring.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/rotamer.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/rotor.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/shared_ptr.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/spectrophore.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/tautomer.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/text.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/tokenst.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/typer.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/xml.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/babelconfig.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel/math" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/math/align.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/math/erf.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/math/matrix3x3.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/math/spacegroup.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/math/transform3d.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/math/vector3.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel/stereo" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/bindings.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/cistrans.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/squareplanar.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/stereo.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/tetrahedral.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/tetranonplanar.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/stereo/tetraplanar.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  FILE(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/openbabel-2.0/openbabel/depict" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/depict/asciipainter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/depict/cairopainter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/depict/commandpainter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/depict/depict.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/depict/painter.h"
    "/home/tru/rpmbuild/BUILD/openbabel-master/python3/include/openbabel/depict/svgpainter.h"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

