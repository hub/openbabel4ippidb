# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Produce verbose output by default.
VERBOSE = 1

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/tru/rpmbuild/BUILD/openbabel-master/python3

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/tru/rpmbuild/BUILD/openbabel-master/python3

# Include any dependencies generated for this target.
include src/formats/CMakeFiles/fastaformat.dir/depend.make

# Include the progress variables for this target.
include src/formats/CMakeFiles/fastaformat.dir/progress.make

# Include the compile flags for this target's objects.
include src/formats/CMakeFiles/fastaformat.dir/flags.make

src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o: src/formats/CMakeFiles/fastaformat.dir/flags.make
src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o: src/formats/fastaformat.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/tru/rpmbuild/BUILD/openbabel-master/python3/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/fastaformat.dir/fastaformat.cpp.o -c /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats/fastaformat.cpp

src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/fastaformat.dir/fastaformat.cpp.i"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats/fastaformat.cpp > CMakeFiles/fastaformat.dir/fastaformat.cpp.i

src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/fastaformat.dir/fastaformat.cpp.s"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats/fastaformat.cpp -o CMakeFiles/fastaformat.dir/fastaformat.cpp.s

src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.requires:
.PHONY : src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.requires

src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.provides: src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.requires
	$(MAKE) -f src/formats/CMakeFiles/fastaformat.dir/build.make src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.provides.build
.PHONY : src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.provides

src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.provides.build: src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o

# Object files for target fastaformat
fastaformat_OBJECTS = \
"CMakeFiles/fastaformat.dir/fastaformat.cpp.o"

# External object files for target fastaformat
fastaformat_EXTERNAL_OBJECTS =

lib64/fastaformat.so: src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o
lib64/fastaformat.so: src/formats/CMakeFiles/fastaformat.dir/build.make
lib64/fastaformat.so: /usr/lib64/libm.so
lib64/fastaformat.so: /usr/lib64/libz.so
lib64/fastaformat.so: /usr/lib64/libcairo.so
lib64/fastaformat.so: /usr/lib64/libinchi.so
lib64/fastaformat.so: lib64/libopenbabel.so.5.0.0
lib64/fastaformat.so: /usr/lib64/libm.so
lib64/fastaformat.so: /usr/lib64/libz.so
lib64/fastaformat.so: src/formats/CMakeFiles/fastaformat.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared module ../../lib64/fastaformat.so"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/fastaformat.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/formats/CMakeFiles/fastaformat.dir/build: lib64/fastaformat.so
.PHONY : src/formats/CMakeFiles/fastaformat.dir/build

src/formats/CMakeFiles/fastaformat.dir/requires: src/formats/CMakeFiles/fastaformat.dir/fastaformat.cpp.o.requires
.PHONY : src/formats/CMakeFiles/fastaformat.dir/requires

src/formats/CMakeFiles/fastaformat.dir/clean:
	cd /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats && $(CMAKE_COMMAND) -P CMakeFiles/fastaformat.dir/cmake_clean.cmake
.PHONY : src/formats/CMakeFiles/fastaformat.dir/clean

src/formats/CMakeFiles/fastaformat.dir/depend:
	cd /home/tru/rpmbuild/BUILD/openbabel-master/python3 && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/tru/rpmbuild/BUILD/openbabel-master/python3 /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats /home/tru/rpmbuild/BUILD/openbabel-master/python3 /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats /home/tru/rpmbuild/BUILD/openbabel-master/python3/src/formats/CMakeFiles/fastaformat.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/formats/CMakeFiles/fastaformat.dir/depend

