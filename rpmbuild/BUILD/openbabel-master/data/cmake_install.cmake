# Install script for directory: /home/tru/rpmbuild/BUILD/openbabel-master/data

# Set the install prefix
IF(NOT DEFINED CMAKE_INSTALL_PREFIX)
  SET(CMAKE_INSTALL_PREFIX "/usr")
ENDIF(NOT DEFINED CMAKE_INSTALL_PREFIX)
STRING(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
IF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  IF(BUILD_TYPE)
    STRING(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  ELSE(BUILD_TYPE)
    SET(CMAKE_INSTALL_CONFIG_NAME "Release")
  ENDIF(BUILD_TYPE)
  MESSAGE(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
ENDIF(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)

# Set the component getting installed.
IF(NOT CMAKE_INSTALL_COMPONENT)
  IF(COMPONENT)
    MESSAGE(STATUS "Install component: \"${COMPONENT}\"")
    SET(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  ELSE(COMPONENT)
    SET(CMAKE_INSTALL_COMPONENT)
  ENDIF(COMPONENT)
ENDIF(NOT CMAKE_INSTALL_COMPONENT)

# Install shared libraries without execute permission?
IF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  SET(CMAKE_INSTALL_SO_NO_EXE "0")
ENDIF(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)

IF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/share/openbabel/2.4.90/atomization-energies.txt;/usr/share/openbabel/2.4.90/atomtyp.txt;/usr/share/openbabel/2.4.90/babel_povray3.inc;/usr/share/openbabel/2.4.90/bondtyp.txt;/usr/share/openbabel/2.4.90/eem.txt;/usr/share/openbabel/2.4.90/eem2015ba.txt;/usr/share/openbabel/2.4.90/eem2015bm.txt;/usr/share/openbabel/2.4.90/eem2015bn.txt;/usr/share/openbabel/2.4.90/eem2015ha.txt;/usr/share/openbabel/2.4.90/eem2015hm.txt;/usr/share/openbabel/2.4.90/eem2015hn.txt;/usr/share/openbabel/2.4.90/eqeqIonizations.txt;/usr/share/openbabel/2.4.90/fragments.txt;/usr/share/openbabel/2.4.90/fragment-index.txt;/usr/share/openbabel/2.4.90/gaff.dat;/usr/share/openbabel/2.4.90/gaff.prm;/usr/share/openbabel/2.4.90/ghemical.prm;/usr/share/openbabel/2.4.90/logp.txt;/usr/share/openbabel/2.4.90/MACCS.txt;/usr/share/openbabel/2.4.90/mmff94.ff;/usr/share/openbabel/2.4.90/mmff94s.ff;/usr/share/openbabel/2.4.90/mmffang.par;/usr/share/openbabel/2.4.90/mmffbndk.par;/usr/share/openbabel/2.4.90/mmffbond.par;/usr/share/openbabel/2.4.90/mmffchg.par;/usr/share/openbabel/2.4.90/mmffdef.par;/usr/share/openbabel/2.4.90/mmffdfsb.par;/usr/share/openbabel/2.4.90/mmffoop.par;/usr/share/openbabel/2.4.90/mmffpbci.par;/usr/share/openbabel/2.4.90/mmffprop.par;/usr/share/openbabel/2.4.90/mmffstbn.par;/usr/share/openbabel/2.4.90/mmfftor.par;/usr/share/openbabel/2.4.90/mmffvdw.par;/usr/share/openbabel/2.4.90/mmffs_oop.par;/usr/share/openbabel/2.4.90/mmffs_tor.par;/usr/share/openbabel/2.4.90/mpC.txt;/usr/share/openbabel/2.4.90/mr.txt;/usr/share/openbabel/2.4.90/patterns.txt;/usr/share/openbabel/2.4.90/phmodel.txt;/usr/share/openbabel/2.4.90/platinum-fragment.txt;/usr/share/openbabel/2.4.90/plugindefines.txt;/usr/share/openbabel/2.4.90/psa.txt;/usr/share/openbabel/2.4.90/qeq.txt;/usr/share/openbabel/2.4.90/resdata.txt;/usr/share/openbabel/2.4.90/ringtyp.txt;/usr/share/openbabel/2.4.90/SMARTS_InteLigand.txt;/usr/share/openbabel/2.4.90/space-groups.txt;/usr/share/openbabel/2.4.90/superatom.txt;/usr/share/openbabel/2.4.90/svgformat.script;/usr/share/openbabel/2.4.90/templates.sdf;/usr/share/openbabel/2.4.90/torlib.txt;/usr/share/openbabel/2.4.90/torsion.txt;/usr/share/openbabel/2.4.90/types.txt;/usr/share/openbabel/2.4.90/UFF.prm")
  IF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
  IF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  ENDIF (CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
FILE(INSTALL DESTINATION "/usr/share/openbabel/2.4.90" TYPE FILE FILES
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/atomization-energies.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/atomtyp.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/babel_povray3.inc"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/bondtyp.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem2015ba.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem2015bm.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem2015bn.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem2015ha.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem2015hm.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eem2015hn.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/eqeqIonizations.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/fragments.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/fragment-index.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/gaff.dat"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/gaff.prm"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/ghemical.prm"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/logp.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/MACCS.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmff94.ff"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmff94s.ff"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffang.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffbndk.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffbond.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffchg.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffdef.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffdfsb.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffoop.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffpbci.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffprop.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffstbn.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmfftor.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffvdw.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffs_oop.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mmffs_tor.par"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mpC.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/mr.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/patterns.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/phmodel.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/platinum-fragment.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/plugindefines.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/psa.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/qeq.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/resdata.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/ringtyp.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/SMARTS_InteLigand.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/space-groups.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/superatom.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/svgformat.script"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/templates.sdf"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/torlib.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/torsion.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/types.txt"
    "/home/tru/rpmbuild/BUILD/openbabel-master/data/UFF.prm"
    )
ENDIF(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")

