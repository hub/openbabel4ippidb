# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.8

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Produce verbose output by default.
VERBOSE = 1

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/tru/rpmbuild/BUILD/openbabel-master

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/tru/rpmbuild/BUILD/openbabel-master

# Include any dependencies generated for this target.
include src/formats/CMakeFiles/reportformat.dir/depend.make

# Include the progress variables for this target.
include src/formats/CMakeFiles/reportformat.dir/progress.make

# Include the compile flags for this target's objects.
include src/formats/CMakeFiles/reportformat.dir/flags.make

src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o: src/formats/CMakeFiles/reportformat.dir/flags.make
src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o: src/formats/reportformat.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/tru/rpmbuild/BUILD/openbabel-master/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/src/formats && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/reportformat.dir/reportformat.cpp.o -c /home/tru/rpmbuild/BUILD/openbabel-master/src/formats/reportformat.cpp

src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/reportformat.dir/reportformat.cpp.i"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/src/formats && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/tru/rpmbuild/BUILD/openbabel-master/src/formats/reportformat.cpp > CMakeFiles/reportformat.dir/reportformat.cpp.i

src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/reportformat.dir/reportformat.cpp.s"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/src/formats && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/tru/rpmbuild/BUILD/openbabel-master/src/formats/reportformat.cpp -o CMakeFiles/reportformat.dir/reportformat.cpp.s

src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.requires:
.PHONY : src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.requires

src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.provides: src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.requires
	$(MAKE) -f src/formats/CMakeFiles/reportformat.dir/build.make src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.provides.build
.PHONY : src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.provides

src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.provides.build: src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o

# Object files for target reportformat
reportformat_OBJECTS = \
"CMakeFiles/reportformat.dir/reportformat.cpp.o"

# External object files for target reportformat
reportformat_EXTERNAL_OBJECTS =

lib64/reportformat.so: src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o
lib64/reportformat.so: src/formats/CMakeFiles/reportformat.dir/build.make
lib64/reportformat.so: /usr/lib64/libm.so
lib64/reportformat.so: /usr/lib64/libz.so
lib64/reportformat.so: /usr/lib64/libcairo.so
lib64/reportformat.so: /usr/lib64/libinchi.so
lib64/reportformat.so: lib64/libopenbabel.so.5.0.0
lib64/reportformat.so: /usr/lib64/libm.so
lib64/reportformat.so: /usr/lib64/libz.so
lib64/reportformat.so: src/formats/CMakeFiles/reportformat.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared module ../../lib64/reportformat.so"
	cd /home/tru/rpmbuild/BUILD/openbabel-master/src/formats && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/reportformat.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/formats/CMakeFiles/reportformat.dir/build: lib64/reportformat.so
.PHONY : src/formats/CMakeFiles/reportformat.dir/build

src/formats/CMakeFiles/reportformat.dir/requires: src/formats/CMakeFiles/reportformat.dir/reportformat.cpp.o.requires
.PHONY : src/formats/CMakeFiles/reportformat.dir/requires

src/formats/CMakeFiles/reportformat.dir/clean:
	cd /home/tru/rpmbuild/BUILD/openbabel-master/src/formats && $(CMAKE_COMMAND) -P CMakeFiles/reportformat.dir/cmake_clean.cmake
.PHONY : src/formats/CMakeFiles/reportformat.dir/clean

src/formats/CMakeFiles/reportformat.dir/depend:
	cd /home/tru/rpmbuild/BUILD/openbabel-master && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/tru/rpmbuild/BUILD/openbabel-master /home/tru/rpmbuild/BUILD/openbabel-master/src/formats /home/tru/rpmbuild/BUILD/openbabel-master /home/tru/rpmbuild/BUILD/openbabel-master/src/formats /home/tru/rpmbuild/BUILD/openbabel-master/src/formats/CMakeFiles/reportformat.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/formats/CMakeFiles/reportformat.dir/depend

