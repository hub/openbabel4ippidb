
  SET(ENV{PYTHONPATH} /home/tru/rpmbuild/BUILD/openbabel-master/scripts/python:/home/tru/rpmbuild/BUILD/openbabel-master/lib64)
  SET(ENV{LD_LIBRARY_PATH} /home/tru/rpmbuild/BUILD/openbabel-master/scripts/python:/home/tru/rpmbuild/BUILD/openbabel-master/lib64:$ENV{LD_LIBRARY_PATH})
  SET(ENV{BABEL_LIBDIR} /home/tru/rpmbuild/BUILD/openbabel-master/lib64/)
  SET(ENV{BABEL_DATADIR} /home/tru/rpmbuild/BUILD/openbabel-master/data)
  MESSAGE("/home/tru/rpmbuild/BUILD/openbabel-master/scripts/python:/home/tru/rpmbuild/BUILD/openbabel-master/lib64")
  EXECUTE_PROCESS(
  	COMMAND /usr/bin/python2 /home/tru/rpmbuild/BUILD/openbabel-master/test/testexample.py 
  	#WORKING_DIRECTORY @LIBRARY_OUTPUT_PATH@
  	RESULT_VARIABLE import_res
  	OUTPUT_VARIABLE import_output
  	ERROR_VARIABLE  import_output
  )
  
  # Pass the output back to ctest
  IF(import_output)
    MESSAGE(${import_output})
  ENDIF(import_output)
  IF(import_res)
    MESSAGE(SEND_ERROR ${import_res})
  ENDIF(import_res)
