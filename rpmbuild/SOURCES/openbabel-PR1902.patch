From 2abe078bfe14b3150c7e29ad92c2f2ea692588f1 Mon Sep 17 00:00:00 2001
From: Fredrik Wallner <fredrik@wallner.nu>
Date: Fri, 19 Oct 2018 17:40:15 +0200
Subject: [PATCH] Updated the coordinate generation from templates. Should fix
 #1900 while keeping #1851 running. A bit hacky though since the mcdlutil
 routines are hard to penetrate...

---
 src/mcdlutil.cpp     | 18 +++++++++---------
 test/testbindings.py | 38 ++++++++++++++++++++++++++++++++++++++
 2 files changed, 47 insertions(+), 9 deletions(-)

diff --git a/src/mcdlutil.cpp b/src/mcdlutil.cpp
index f202a8b96..e2a4b1503 100644
--- a/src/mcdlutil.cpp
+++ b/src/mcdlutil.cpp
@@ -2568,8 +2568,8 @@ namespace OpenBabel {
               ux1=getAtom(dsNA1[i])->rx-ux;
               uy1=getAtom(dsNA1[i])->ry-uy;
               //coordinates of all atoms for the cycle under study are calculated}
-
-              for (j=0; j<nb-1; j++) {
+              if ((nb + i) > dsATN.size()) nb=dsATN.size()-i; // Check added to avoid segfault below, see issue #1851
+              for (j=0; j<nb; j++) {
                 getAtom(dsATN[i+j-0])->rx=ux+ux1*cos((j+1)*fi)+uy1*sin((j+1)*fi);
                 getAtom(dsATN[i+j-0])->ry=uy-ux1*sin((j+1)*fi)+uy1*cos((j+1)*fi);
                 atomDefine[dsATN[i+j-0]]=1;
@@ -3076,7 +3076,7 @@ namespace OpenBabel {
       TSingleBond* dblbond = sm.getBond(i);
       if (dblbond->bstereo_refs.empty())
         continue;
-    
+
       an1 = dblbond->at[0];
       an2 = dblbond->at[1];
 
@@ -5385,7 +5385,7 @@ namespace OpenBabel {
         };
     };
   };
-  
+
   void TemplateRedraw::rescaleSingleFragment(TSimpleMolecule * sm, std::vector<int>* atomList, PartFragmentDefinition& pf, double offset) {
     int i,n;
     double xMin,xMax,yMin,yMax;
@@ -5404,7 +5404,7 @@ namespace OpenBabel {
       if ((sm->getAtom(n)->ry > yMax) || (yMax == RUNDEF)) yMax=sm->getAtom(n)->ry;
     };
     // LPW: Nearly horizontal molecules were being scaled which resulted in
-    // some nonsensical structures.  This code ensures that coordinates with 
+    // some nonsensical structures.  This code ensures that coordinates with
     // "almost" horizontal or vertical geometries don't get scaled.
     bool Xeq = (fabs(xMax - xMin) < 0.01);
     bool Yeq = (fabs(yMax - yMin) < 0.01);
@@ -5751,11 +5751,11 @@ namespace OpenBabel {
               test=true;
               emTemplate=new TEditedMolecule();
 			  if (tm.getBond(i)->at[1] < nFound) {
-				templateAN=tm.getBond(i)->at[0]; 
-				fragmentAN=tm.getBond(i)->at[1]; 
+				templateAN=tm.getBond(i)->at[0];
+				fragmentAN=tm.getBond(i)->at[1];
 			  } else {
 			    templateAN=tm.getBond(i)->at[1];
-				fragmentAN=tm.getBond(i)->at[0]; 
+				fragmentAN=tm.getBond(i)->at[0];
 			  }
               templateAtomNumber.push_back(tm.getAtom(templateAN)->enumerator);  //in template....
               fragmentAtomNumber.push_back(tm.getAtom(fragmentAN)->enumerator);
@@ -5777,7 +5777,7 @@ namespace OpenBabel {
       atomClean=tm.nAtoms()-nFound;
       listAtomClean.resize(0);
       for (i=0; i<atomClean; i++) listAtomClean.push_back(nFound+i);
-      
+
       vector<int> listBondClean;
       bondClean=0;
       for (i=0; i<tm.nBonds(); i++) {
diff --git a/test/testbindings.py b/test/testbindings.py
index 311f3b2a8..ceaff1d9e 100644
--- a/test/testbindings.py
+++ b/test/testbindings.py
@@ -484,6 +484,44 @@ def testProper2DofFragments(self):
         mindist = min(dists)
         self.assertTrue(mindist > 0.00001)
 
+    def testRegressionBenzene2D(self):
+        """Check that benzene is given a correct layout, see #1900"""
+        mol = pybel.readstring("smi", "c1ccccc1")
+        mol.draw(show=False, update=True)
+        benzmol = """
+ OpenBabel10161813072D
+
+  6  6  0  0  0  0  0  0  0  0999 V2000
+   -0.8660   -0.5000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
+   -1.7321   -0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
+   -1.7321    1.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
+   -0.8660    1.5000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
+   -0.0000    1.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
+    0.0000    0.0000    0.0000 C   0  0  0  0  0  0  0  0  0  0  0  0
+  1  6  2  0  0  0  0
+  1  2  1  0  0  0  0
+  2  3  2  0  0  0  0
+  3  4  1  0  0  0  0
+  4  5  2  0  0  0  0
+  5  6  1  0  0  0  0
+M  END
+"""
+        self.assertEqual(mol.write("mol")[24:], benzmol[24:])
+
+    def testTemplates(self):
+        """Check for regressions to #1851"""
+        smis = [
+            "O=C(C1=CN=CS1)N1C2CCC1CN(CC1CC3CCC1O3)C2",
+            "O=C(CC1CC1)N1C2CCC1CC(NC(=O)C13CCC(CC1)CC3)C2",
+            "O=C([C@@H]1C[C@H]1C1CCC1)N1C2CCC1CN(C(=O)C13CCN(CC1)C3)C2",
+            "O=C(CCN1C=CN=C1)N1C2CCC1CN(CC1CC3CCC1C3)C2"
+            ]
+        for s in smis:
+            mol = pybel.readstring("smi", s)
+            mol.draw(show=False, update=True)
+        assert(True) # Segfaults before...
+
+
 class NewReactionHandling(PythonBindings):
 
     def testBasic(self):
